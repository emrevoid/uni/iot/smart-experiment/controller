/*
   Sistemi Embedded e IoT 2020-2021
   Assignment #2: Smart Experiment
   Author:
   - Andrea Casadei 800898
*/

#ifndef BUTTONIMPL_H
#define BUTTONIMPL_H

#include "Arduino.h"
#include "Button.h"

class ButtonImpl : public Button
{
  public:
    explicit ButtonImpl(unsigned pin);
    bool isPressed() override;

  private:
    unsigned pin;
};

#endif