/*
   Sistemi Embedded e IoT 2020-2021
   Assignment #2: Smart Experiment
   Author:
   - Andrea Casadei 800898
*/

#ifndef ENDTASK_H
#define ENDTASK_H

#include "Arduino.h"
#include "SmartExpGlobal.h"
#include "Task.h"

class EndTask : public Task
{
  public:
    EndTask();
    void init(int period);
    void tick() override;
};

#endif