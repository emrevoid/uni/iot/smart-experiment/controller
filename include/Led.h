/*
   Sistemi Embedded e IoT 2020-2021
   Assignment #2: Smart Experiment
   Author:
   - Andrea Casadei 800898
*/

#ifndef LED_H
#define LED_H

#include "Light.h"

class Led : public Light
{
  public:
    void switchOn() override;
    void switchOff() override;
    bool isTurnedOn() override;
};

#endif