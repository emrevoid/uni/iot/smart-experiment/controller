/*
   Sistemi Embedded e IoT 2020-2021
   Assignment #2: Smart Experiment
   Author:
   - Andrea Casadei 800898
*/

#ifndef LIGHT_H
#define LIGHT_H

class Light
{
  public:
    virtual void switchOn() = 0;
    virtual void switchOff() = 0;
    virtual bool isTurnedOn() = 0;
};

#endif