/*
   Sistemi Embedded e IoT 2020-2021
   Assignment #2: Smart Experiment
   Author:
   - Andrea Casadei 800898
*/

#ifndef MODE_H
#define MODE_H

enum Modes
{
    WELCOME,
    SLEEP,
    RUN,
    SCAN,
    END,
    STARTEXP,
    STOPEXP
};

#endif