/*
   Sistemi Embedded e IoT 2020-2021
   Assignment #2: Smart Experiment
   Author:
   - Andrea Casadei 800898
*/

#ifndef MOTIONSENSOR_H
#define MOTIONSENSOR_H

class MotionSensor
{
  public:
    virtual bool read() = 0;
};

#endif