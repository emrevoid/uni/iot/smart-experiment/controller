/*
   Sistemi Embedded e IoT 2020-2021
   Assignment #2: Smart Experiment
   Author:
   - Andrea Casadei 800898
*/

#ifndef MSGSERVICE_H
#define MSGSERVICE_H

#include "Arduino.h"

class Msg
{
    String content;

  public:
    /**
     * Instanciates a message.
     * @param content Message content.
     */
    explicit Msg(String content) { this->content = content; }

    /**
     * Get the message content.
     * @return the message content.
     */
    String getContent() { return content; }
};

/**
 * A matching pattern for a Message.
 * @see Msg
 */
class Pattern
{
  public:
    /**
     * Match the pattern.
     * @return if the pattern matches.
     */
    virtual boolean match(const Msg& m) = 0;
};

/**
 * A controler for sending and receiving messages.
 * @see Msg
 */
class MsgServiceClass
{
  public:
    Msg* currentMsg;
    bool msgAvailable;

    /**
     * Initialize the I/O message controller.
     */
    void init();

    /**
     * Check if a message is available.
     * @return if a message is available.
     */
    bool isMsgAvailable();

    /**
     * Receive and save a message.
     * @see Msg
     */
    Msg* receiveMsg();

    /**
     * @copydoc MsgServiceClass::isMsgAvailable()
     * with a given message pattern to be matched.
     */
    bool isMsgAvailable(Pattern& pattern);

    /**
     * MsgServiceClass::receiveMsg()
     * with a given message pattern to be matched.
     */
    Msg* receiveMsg(Pattern& pattern);

    /**
     * Send a message.
     * @param msg the message to be sent.
     */
    void sendMsg(const String& msg);
};

extern MsgServiceClass MsgService;

#endif