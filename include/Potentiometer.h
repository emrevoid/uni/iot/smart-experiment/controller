/*
   Sistemi Embedded e IoT 2020-2021
   Assignment #2: Smart Experiment
   Author:
   - Andrea Casadei 800898
*/

#ifndef POTENTIOMETER_H
#define POTENTIOMETER_H

class Potentiometer
{
  public:
    virtual unsigned read() = 0;
};

#endif