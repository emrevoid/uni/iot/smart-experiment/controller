/*
   Sistemi Embedded e IoT 2020-2021
   Assignment #2: Smart Experiment
   Author:
   - Andrea Casadei 800898
*/

#ifndef POTENTIOMETERIMPL_H
#define POTENTIOMETERIMPL_H

#include "Potentiometer.h"
#include <Arduino.h>

class PotentiometerImpl : public Potentiometer
{
    unsigned pin;
    unsigned min;
    unsigned max;

  public:
    PotentiometerImpl(unsigned pin, unsigned min, unsigned max);
    unsigned read() override;
};

#endif