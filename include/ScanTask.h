/*
   Sistemi Embedded e IoT 2020-2021
   Assignment #2: Smart Experiment
   Author:
   - Andrea Casadei 800898
*/

#ifndef SCANTASK_H
#define SCANTASK_H

#include "ServoMotor.h"
#include "SmartExpGlobal.h"
#include "Task.h"
#include "UltrasonicSensor.h"

class ScanTask : public Task
{
    ServoMotor* motor;
    unsigned pinMotor;
    UltrasonicSensor* sonar;
    unsigned pinEcho;
    unsigned pinTrigger;

  public:
    ScanTask(unsigned pinMotor, unsigned pinEcho, unsigned pinTrigger);
    void init(int period);
    void tick();
};

#endif