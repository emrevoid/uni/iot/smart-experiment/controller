/*
   Sistemi Embedded e IoT 2020-2021
   Assignment #2: Smart Experiment
   Author:
   - Andrea Casadei 800898
*/

#ifndef SCHEDULER_H
#define SCHEDULER_H

#include "Task.h"
#include "Timer.h"
#define MAX_TASKS 10

class Scheduler
{
    int basePeriod;
    int taskCount;
    Task* taskList[MAX_TASKS];
    Timer timer;

  public:
    /**
     * Initialize the scheduler.
     * @param basePeriod the scheduler period.
     */
    void init(int basePeriod);

    /**
     * Add a task to be executed by the scheduler.
     * @param task the task to be executed by the scheduler.
     * @return if the task has been added to the scheduler.
     */
    virtual bool addTask(Task* task);

    /**
     * Execute all the added tasks once.
     */
    virtual void schedule();
};

#endif