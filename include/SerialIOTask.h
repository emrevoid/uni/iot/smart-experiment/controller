/*
   Sistemi Embedded e IoT 2020-2021
   Assignment #2: Smart Experiment
   Author:
   - Andrea Casadei 800898
*/

#ifndef SERIALIOTASK_H
#define SERIALIOTASK_H

#include "MsgService.h"
#include "Task.h"

class SerialIOTask : public Task
{
    MsgServiceClass* msgService;

  public:
    SerialIOTask();
    void init(int period);
    void tick() override;
};

#endif