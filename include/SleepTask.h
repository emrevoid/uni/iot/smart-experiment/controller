/*
   Sistemi Embedded e IoT 2020-2021
   Assignment #2: Smart Experiment
   Author:
   - Andrea Casadei 800898
*/

#ifndef SLEEPTASK_H
#define SLEEPTASK_H

#include "Arduino.h"
#include "Button.h"
#include "SmartExpGlobal.h"
#include "Task.h"

class SleepTask : public Task
{
    static unsigned int pin;

  public:
    SleepTask(unsigned int pin);
    void init(int period);
    void tick() override;

  private:
    static void wakeUp();
};

#endif