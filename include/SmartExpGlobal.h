/*
   Sistemi Embedded e IoT 2020-2021
   Assignment #2: Smart Experiment
   Author:
   - Andrea Casadei 800898
*/

#ifndef SMARTEXPGLOBAL_H
#define SMARTEXPGLOBAL_H

#include "Arduino.h"
#include "Led.h"
#include "LedImpl.h"
#include "Mode.h"
#include "Potentiometer.h"
#include "Timer.h"
#include <LinkedList.h>

class SmartExpGlobal
{
  public:
    static Led* led_green;
    static Led* led_red;
    static Modes mode;

    static unsigned errorTime;
    static unsigned sleepTime;
    static unsigned maxTime;
    static unsigned minFreq;
    static unsigned maxFreq;
    static unsigned maxSpeed;

    static float temp;
    static float d_old;
    static float t_old;

    static float d_graph;
    static float t_graph;
    static float v_graph;
    static float a_graph;
    
    static bool gotInput;



    // old, to remove
    static bool startExp;
    static bool endExp;
    static bool expTimeOver;
    static double distance;
    static bool startScan;
    static uint8_t scanFrequency;
    static bool errorState;
    static bool welcomeState;
    
    static bool beginNew;
    static bool allowedToDetect;
    static int nSlices;
    static bool isMoving;
    static bool directionRight;
    static bool objectDetected;
    static bool shouldScan;
    static bool isAlarmState;
    static bool isTrackingState;
    static const int scanDurationMin;
    static const int scanDurationMax;
    static int scanDuration;
    static bool modeChanged;
    static bool shouldGoToSleep;
    static unsigned sliceDelay;
    static float currentAngle;

    // IO string
    static String consoleCommandString;
    static String detectingString;
    static String controllerCommandString;
    static String movementCommandString;
    static String modeCommandString;
    static String movementResponseString;
    static String movementLeftString;
    static String movementRightString;
    static String modeAutoString;
    static String modeSingleString;
    static String modeManualString;
    static String alarmString;
    static String trackingString;
    static String scanDurationCommandString;
    static String detectionString;
    static LinkedList<String> serialOutputMessages;
};

#endif