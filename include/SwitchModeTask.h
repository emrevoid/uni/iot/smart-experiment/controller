/*
   Sistemi Embedded e IoT 2020-2021
   Assignment #2: Smart Experiment
   Author:
   - Andrea Casadei 800898
*/

#ifndef SWITCHMODETASK_H
#define SWITCHMODETASK_H

#include "Arduino.h"
#include "Button.h"
#include "Mode.h"
#include "Task.h"

class SwitchModeTask : public Task
{
    Button* b1;
    Button* b2;

  public:
    SwitchModeTask(unsigned pinB1, unsigned pinB2);
    void init(int period);
    void tick() override;
};

#endif