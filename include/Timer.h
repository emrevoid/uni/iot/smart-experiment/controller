/*
   Sistemi Embedded e IoT 2020-2021
   Assignment #2: Smart Experiment
   Author:
   - Andrea Casadei 800898
*/

#ifndef TIMER_H
#define TIMER_H

class Timer
{
  public:
    /**
     * Instanciates the timer.
     */
    Timer();

    /**
     * Set the timer's frequency.
     * @param frequency timer frequency
     */
    void setupFrequency(int frequency);

    /**
     * Se the timer's period (in ms).
     * @param period timer period (in ms)
     */
    void setupPeriod(int period);

    /**
     * Wait for next tick.
     */
    void waitForNextTick();
};

#endif