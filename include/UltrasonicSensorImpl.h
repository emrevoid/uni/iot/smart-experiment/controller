/*
   Sistemi Embedded e IoT 2020-2021
   Assignment #2: Smart Experiment
   Author:
   - Andrea Casadei 800898
*/

#ifndef ULTRASONICSENSORIMPL_H
#define ULTRASONICSENSORIMPL_H

#include "UltrasonicSensor.h"
#include <Arduino.h>

class UltrasonicSensorImpl : public UltrasonicSensor
{
  private:
    const unsigned int firstDelay = 3;
    const unsigned int secondDelay = 5;
    const double timeScale = 1 / 1000.0 / 1000.0 / 2;
    unsigned triggerPin;
    unsigned echoPin;
    double soundSpeed;

  public:
    UltrasonicSensorImpl(unsigned triggerPin, unsigned echoPin);
    double read();
};

#endif