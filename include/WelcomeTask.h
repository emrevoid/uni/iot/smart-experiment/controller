/*
   Sistemi Embedded e IoT 2020-2021
   Assignment #2: Smart Experiment
   Author:
   - Andrea Casadei 800898
*/

#ifndef WELCOMETASK_H
#define WELCOMETASK_H

#include "SmartExpGlobal.h"
#include "Task.h"

class WelcomeTask : public Task
{
  public:
    WelcomeTask();
    void init(int period);
    void tick();
};

#endif