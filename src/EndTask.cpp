/*
   Sistemi Embedded e IoT 2020-2021
   Assignment #2: Smart Experiment
   Author:
   - Andrea Casadei 800898
*/

#include "EndTask.h"
#include "SmartExpGlobal.h"

EndTask::EndTask() {}

void
EndTask::init(int period)
{
    Task::init(period);
}

void
EndTask::tick()
{
    if (SmartExpGlobal::mode == Modes::END) {
        /**
        do {
            SmartExpGlobal::led_green->switchOn();
            delay(SmartExpGlobal::errorTime / 2);
            SmartExpGlobal::led_green->switchOff();
            delay(SmartExpGlobal::errorTime / 2);
        } while (!SmartExpGlobal::gotInput);
        **/
        // Serial.println("exp ended, waiting for input..");
        for (int i = 0; i < 5; i++) {
            SmartExpGlobal::led_green->switchOn();
            delay(SmartExpGlobal::errorTime / 2);
            SmartExpGlobal::led_green->switchOff();
            delay(SmartExpGlobal::errorTime / 2);
        }

        SmartExpGlobal::mode = Modes::WELCOME;
    }
}