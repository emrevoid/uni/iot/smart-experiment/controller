/*
   Sistemi Embedded e IoT 2020-2021
   Assignment #2: Smart Experiment
   Author:
   - Andrea Casadei 800898
*/

#include "LedImpl.h"

LedImpl::LedImpl(unsigned pin)
{
    this->pin = pin;
    digitalWrite(pin, LOW);
    isOn = false;
}

void
LedImpl::switchOn()
{
    digitalWrite(pin, HIGH);
    isOn = true;
}

void
LedImpl::switchOff()
{
    digitalWrite(pin, LOW);
    isOn = false;
}

bool
LedImpl::isTurnedOn()
{
    return isOn;
}
