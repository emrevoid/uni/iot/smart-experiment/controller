/*
   Sistemi Embedded e IoT 2020-2021
   Assignment #2: Smart Experiment
   Author:
   - Andrea Casadei 800898
*/

#include "MotionSensorImpl.h"

MotionSensorImpl::MotionSensorImpl(unsigned pin)
  : calibrationDuration(10)
{
    this->pin = pin;
}

bool
MotionSensorImpl::read()
{
    return (digitalRead(pin) == HIGH);
}
