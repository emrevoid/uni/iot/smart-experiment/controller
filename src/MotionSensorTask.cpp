/*
   Sistemi Embedded e IoT 2020-2021
   Assignment #2: Smart Experiment
   Author:
   - Andrea Casadei 800898
*/

#include "MotionSensorTask.h"
#include "MotionSensorImpl.h"

MotionSensorTask::MotionSensorTask(unsigned pin)
{
    this->pin = pin;
}

void
MotionSensorTask::init(int period)
{
    Task::init(period);
    this->motionSensor = new MotionSensorImpl(pin);
}

void
MotionSensorTask::tick()
{
    if (SmartExpGlobal::allowedToDetect) {
        SmartExpGlobal::objectDetected = motionSensor->read();
    }
}
