/*
   Sistemi Embedded e IoT 2020-2021
   Assignment #2: Smart Experiment
   Author:
   - Andrea Casadei 800898
*/

#include "MsgService.h"
#include "Arduino.h"

String content;

MsgServiceClass MsgService;

bool
MsgServiceClass::isMsgAvailable()
{
    return msgAvailable;
}

Msg*
MsgServiceClass::receiveMsg()
{
    if (msgAvailable) {
        Msg* msg = currentMsg;
        msgAvailable = false;
        currentMsg = nullptr;
        content = "";
        return msg;
    } else {
        return nullptr;
    }
}

void
MsgServiceClass::init()
{
    unsigned long baduRate = 9600;
    Serial.begin(baduRate);
    content.reserve(256);
    content = "";
    currentMsg = NULL;
    msgAvailable = false;
}

void
MsgServiceClass::sendMsg(const String& msg)
{
    Serial.println(msg);
}

void
serialEvent()
{
    /* reading the content */
    while (Serial.available()) {
        char ch = (char)Serial.read();
        if (ch == '\n') {
            MsgService.currentMsg = new Msg(content);
            MsgService.msgAvailable = true;
        } else {
            content += ch;
        }
    }
}

bool
MsgServiceClass::isMsgAvailable(Pattern& pattern)
{
    return (msgAvailable && pattern.match(*currentMsg));
}

Msg*
MsgServiceClass::receiveMsg(Pattern& pattern)
{
    if (msgAvailable && pattern.match(*currentMsg)) {
        Msg* msg = currentMsg;
        msgAvailable = false;
        currentMsg = nullptr;
        content = "";
        return msg;
    } else {
        return nullptr;
    }
}
