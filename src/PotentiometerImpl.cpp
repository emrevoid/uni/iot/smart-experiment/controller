/*
   Sistemi Embedded e IoT 2020-2021
   Assignment #2: Smart Experiment
   Author:
   - Andrea Casadei 800898
*/

#include "PotentiometerImpl.h"

PotentiometerImpl::PotentiometerImpl(unsigned pin, unsigned min, unsigned max)
{
    pinMode(pin, INPUT);
    this->pin = pin;
    this->min = min;
    this->max = max;
}

unsigned
PotentiometerImpl::read()
{
    uint8_t valueRead = analogRead(pin) + 1;
    unsigned int range = 256 / (this->max - this->min);
    valueRead = valueRead / range + this->min;
    return valueRead;
}
