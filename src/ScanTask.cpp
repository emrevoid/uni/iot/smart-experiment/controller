/*
   Sistemi Embedded e IoT 2020-2021
   Assignment #2: Smart Experiment
   Author:
   - Andrea Casadei 800898
*/

#include "ScanTask.h"
#include "ServoMotorImpl.h"
#include "SmartExpGlobal.h"
#include "UltrasonicSensorImpl.h"

ScanTask::ScanTask(unsigned pinMotor, unsigned pinEcho, unsigned pinTrigger)
{
    this->pinMotor = pinMotor;
    this->pinEcho = pinEcho;
    this->pinTrigger = pinTrigger;
}

void
ScanTask::init(int period)
{
    Task::init(period);
    this->motor = new ServoMotorImpl(pinMotor);
    this->sonar = new UltrasonicSensorImpl(this->pinTrigger, this->pinEcho);
}

void
ScanTask::tick()
{
    if (SmartExpGlobal::mode == Modes::SCAN) {
        float d1, d2, t1, t2, v, a = 0.0;

        d1 = SmartExpGlobal::d_old;
        t1 = SmartExpGlobal::t_old;
        d2 = sonar->read();
        t2 = millis();
        v = (d2 - d1) / (t1 - t2);
        a = v / (t2 - t1);

        SmartExpGlobal::d_old = d2;
        SmartExpGlobal::t_old = t2;
        SmartExpGlobal::d_graph = d2;
        SmartExpGlobal::t_graph = t2;
        SmartExpGlobal::v_graph = v;
        SmartExpGlobal::a_graph = a;

        // Serial.println("calculating physics");
        // Serial.println("moving servo");
        /**
                if (false) {
                    SmartExpGlobal::mode = Modes::STOPEXP;
                }
        **/
    }

    if (SmartExpGlobal::mode == Modes::STOPEXP) {
        SmartExpGlobal::mode = Modes::END;
    }
}
