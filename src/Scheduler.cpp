/*
   Sistemi Embedded e IoT 2020-2021
   Assignment #2: Smart Experiment
   Author:
   - Andrea Casadei 800898
*/

#include "Scheduler.h"

void
Scheduler::init(int basePeriod)
{
    this->basePeriod = basePeriod;
    timer.setupPeriod(basePeriod);
    taskCount = 0;
}

bool
Scheduler::addTask(Task* task)
{
    if (taskCount < MAX_TASKS - 1) {
        taskList[taskCount] = task;
        taskCount++;
        return true;
    } else {
        return false;
    }
}

void
Scheduler::schedule()
{
    timer.waitForNextTick();
    for (int i = 0; i < taskCount; i++) {
        if (taskList[i]->updateAndCheckTime(basePeriod)) {
            taskList[i]->tick();
        }
    }
}
