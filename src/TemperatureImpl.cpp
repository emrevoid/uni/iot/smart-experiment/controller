/*
   Sistemi Embedded e IoT 2020-2021
   Assignment #2: Smart Experiment
   Author:
   - Andrea Casadei 800898
*/

#define VCC ((float)5)

#include "TemperatureImpl.h"

TemperatureImpl::TemperatureImpl(unsigned pin)
{
    this->pin = pin;
}

unsigned
TemperatureImpl::read()
{
    int valueRead = analogRead(pin);

    float vVolt = valueRead * VCC / 1023;
    float vCelsius = vVolt / 0.01;
    float vKelvin = vCelsius + 273.0;

    return vKelvin;
}
