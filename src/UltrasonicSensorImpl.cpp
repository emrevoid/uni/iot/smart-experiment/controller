/*
   Sistemi Embedded e IoT 2020-2021
   Assignment #2: Smart Experiment
   Author:
   - Andrea Casadei 800898
*/

#include "UltrasonicSensorImpl.h"
#include "SmartExpGlobal.h"

UltrasonicSensorImpl::UltrasonicSensorImpl(unsigned triggerPin,
                                           unsigned echoPin)
{
    pinMode(triggerPin, OUTPUT);
    pinMode(echoPin, INPUT);
    this->triggerPin = triggerPin;
    this->echoPin = echoPin;
}

double
UltrasonicSensorImpl::read()
{
    soundSpeed = sqrt(401.8 * SmartExpGlobal::temp);

    // Invio impulso
    digitalWrite(triggerPin, LOW);
    delayMicroseconds(firstDelay);
    digitalWrite(triggerPin, HIGH);
    delayMicroseconds(secondDelay);
    digitalWrite(triggerPin, LOW);

    // Ricevi l’eco
    unsigned long timeMicroseconds = pulseIn(echoPin, HIGH, 1000000UL);
    double time = timeMicroseconds * timeScale;
    double distance = time * soundSpeed;

    return distance;
}
