/*
   Sistemi Embedded e IoT 2020-2021
   Assignment #2: Smart Experiment
   Author:
   - Andrea Casadei 800898
*/

#include "WelcomeTask.h"
#include "Arduino.h"

WelcomeTask::WelcomeTask() {}

void
WelcomeTask::init(int period)
{
    Task::init(period);
}

void
WelcomeTask::tick()
{
    if (SmartExpGlobal::mode == Modes::WELCOME) {
        // Serial.println("enter welcome, turn on led");
        SmartExpGlobal::led_red->switchOn();
        SmartExpGlobal::led_green->switchOff();
    }
/**
    if (!SmartExpGlobal::objectDetected) {
        Serial.println("going to sleep");
        // delay(5000);
        SmartExpGlobal::mode = Modes::SLEEP;
    }**/
}
