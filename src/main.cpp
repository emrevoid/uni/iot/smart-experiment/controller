/*
   Sistemi Embedded e IoT 2020-2021
   Assignment #2: Smart Experiment
   Author:
   - Andrea Casadei 800898
*/

#include "EndTask.h"
#include "Led.h"
#include "LedImpl.h"
#include "RunTask.h"
#include "ScanTask.h"
#include "Scheduler.h"
#include "SerialIOTask.h"
#include "SleepTask.h"
#include "SmartExpGlobal.h"
#include "SwitchModeTask.h"
#include "WelcomeTask.h"
#include <Arduino.h>

#include "MotionSensorTask.h"

#define PIN_POT A0
#define PIN_B1 5
#define PIN_B2 6
#define PIN_TEMP A1
#define PIN_PIR 2
#define PIN_MOTOR 10
#define PIN_SONAR_TRIG 12
#define PIN_SONAR_ECHO 13

Scheduler scheduler;

void
setup()
{
    Serial.begin(9600);
    scheduler.init(200);

    SerialIOTask* sioTask = new SerialIOTask();
    sioTask->init(50);

    SwitchModeTask* switchTask = new SwitchModeTask(PIN_B1, PIN_B2);
    switchTask->init(50);

    WelcomeTask* welcomeTask = new WelcomeTask();
    welcomeTask->init(50);

    /**
        MotionSensorTask* motionSensorTask = new MotionSensorTask(PIN_PIR);
        motionSensorTask->init(50);
        SleepTask* sleepTask = new SleepTask(PIN_PIR);
        sleepTask->init(50);
    **/

    RunTask* runTask = new RunTask(PIN_SONAR_ECHO,
                                   PIN_SONAR_TRIG,
                                   PIN_POT,
                                   SmartExpGlobal::minFreq,
                                   SmartExpGlobal::maxFreq,
                                   PIN_TEMP);
    runTask->init(50);

    ScanTask* scanTask =
      new ScanTask(PIN_MOTOR, PIN_SONAR_ECHO, PIN_SONAR_TRIG);
    scanTask->init(200);

    EndTask* endTask = new EndTask();
    endTask->init(50);

    scheduler.addTask(sioTask);
    scheduler.addTask(switchTask);
    scheduler.addTask(welcomeTask);
    // scheduler.addTask(sleepTask);
    scheduler.addTask(runTask);
    scheduler.addTask(scanTask);
    scheduler.addTask(endTask);
}

void
loop()
{
    scheduler.schedule();
}